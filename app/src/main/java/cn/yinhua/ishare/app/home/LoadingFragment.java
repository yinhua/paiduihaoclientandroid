package cn.yinhua.ishare.app.home;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;


import cn.yinhua.ishare.app.R;

/**
 * Created by Edward on 6/11/15.
 */
public class LoadingFragment extends Fragment {
    private Context context;

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {



        WebView webView = (WebView)inflater.inflate(R.layout.loading_gif, container, false);

       /*
        WebView lwv = (WebView) loadingFragment.findViewById(R.id.favoritesLoading); */

        String gifFilePath = "file:///android_asset/loading.gif";
        //"file://" + "/drawable/loading_bc.gif";

        String data = "<HTML><Div align=\"center\"  margin=\"0px\"><IMG src=\""+gifFilePath+"\" margin=\"0px\"/></Div>";
        webView.loadDataWithBaseURL(gifFilePath, data, "text/html", "utf-8", null);
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);


        return webView;

    }


}
