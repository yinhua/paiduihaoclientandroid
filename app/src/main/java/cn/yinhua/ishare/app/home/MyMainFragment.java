package cn.yinhua.ishare.app.home;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;

import cn.yinhua.ishare.app.MainActivity;
import cn.yinhua.ishare.app.R;

/**
 * Created by Edward on 7/14/15.
 */
public class MyMainFragment extends Fragment {

    private MainActivity activity;

    public void setActivity(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        final  SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
        String author = sp.getString(activity.getApplicationContext().getResources().getString(R.string.preference_user_login_id), "null");
        //final String author = "edjon";
        if(author.equals("null")){
            // not login
            LinearLayout login_ll = (LinearLayout)inflater.inflate(R.layout.my_login, container, false);
            Button button = (Button)login_ll.findViewById(R.id.login_button_id);

            LinearLayout phoneNum_ll = (LinearLayout)login_ll.findViewById(R.id.phone_number_ll);
            final EditText phoneNumTxt = (EditText)phoneNum_ll.findViewById(R.id.phone_number_et);

            LinearLayout userName_ll = (LinearLayout)login_ll.findViewById(R.id.user_name_ll);
            final EditText userName = (EditText)userName_ll.findViewById(R.id.user_name_et);

            TelephonyManager tMgr =(TelephonyManager)activity.getSystemService(Context.TELEPHONY_SERVICE);
            String mPhoneNumber = tMgr.getLine1Number();

            if(mPhoneNumber != null  && mPhoneNumber.length() > 10){
                mPhoneNumber = mPhoneNumber.replace("+86", "");
                phoneNumTxt.setText(mPhoneNumber);
            }

            button.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    String phoneNum =  phoneNumTxt.getText().toString();
                    String uid = userName.getText().toString();
                    if(phoneNum == null || phoneNum.length() != 11){
                        Toast.makeText(activity.getApplicationContext(), "请填写正确的手机号！", Toast.LENGTH_LONG).show();
                        return;
                    }

                    if(uid == null || uid.length()<2){
                        Toast.makeText(activity.getApplicationContext(), "请填写昵称！", Toast.LENGTH_LONG).show();
                        return;
                    }

                    new LoginTask( activity, phoneNum,  uid, sp).execute();
                }
            });

            return login_ll;

        }else{

            LinearLayout my_main_ll = (LinearLayout)inflater.inflate(R.layout.my_main, container, false);
            LinearLayout head = (LinearLayout)my_main_ll.findViewById(R.id.user_info_head);
            TextView nickNameTV = (TextView)head.findViewById(R.id.userNickName_tv);
            nickNameTV.setText(author);

            TextView logout_tv = (TextView)head.findViewById(R.id.logout_tv);
            logout_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sp.edit().remove(getResources().getString(R.string.preference_user_login_id)).commit();
                    activity.getSupportActionBar().setSelectedNavigationItem(0);
                    activity.getSupportActionBar().setSelectedNavigationItem(1);
                }
            });



            String prefInsKey = getResources().getString(R.string.preference_favorites_ins_set);

            final String favorites = sp.getString(prefInsKey, null);
            final ArrayList<Institution> data = new ArrayList<Institution>();


            if (favorites != null && favorites.length() != 0){

                String[] favoritesItems = favorites.split("\\|");
                for(String favoItem : favoritesItems){

                    String[] insInfArr = favoItem.split("\\$");

                    String insAT = insInfArr[0];
                    String name = insInfArr[1];
                    String address =  insInfArr[2];
                    String subs = insInfArr[3];
                    String register = insInfArr[4];
                    Institution newIns = new Institution();
                    newIns.setAc(insAT.split("-")[0]);
                    newIns.setIt(insAT.split("-")[1]);
                    newIns.setIn(name);
                    newIns.setIa(address);
                    newIns.setSubs(subs);
                    newIns.setIr(register);


                    data.add(newIns);

                }
            }

            final ListView favoriates = (ListView)my_main_ll.findViewById(R.id.myFavor_lv);
            favoriates.setAdapter(new InstListAdapter(activity, data));



            final Resources resources = activity.getResources();

            favoriates.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                    FavorInsDeleteDialog deleteDialog = new FavorInsDeleteDialog();
                    deleteDialog.setFavoriates(favoriates);
                    deleteDialog.setPosition(position);
                    deleteDialog.setResources(resources);
                    deleteDialog.setSp(sp);


                    FragmentManager fragmentManager = activity.getSupportFragmentManager();
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.add(deleteDialog, "");
                    ft.commitAllowingStateLoss();


                    return true;
                }
            });
            return my_main_ll;
        }


    }
}
