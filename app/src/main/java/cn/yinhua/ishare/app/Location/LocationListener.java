package cn.yinhua.ishare.app.Location;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;

/**
 * Created by Edward on 8/14/14.
 */
public class LocationListener implements BDLocationListener {

    private Activity activity;

    public LocationListener(Activity activity){
        this.activity = activity;
    }

    @Override
    public void onReceiveLocation(BDLocation location) {

        if (location == null)
            return;
        StringBuilder sb = new StringBuilder(256);
        sb.append("\ntime : ");
        sb.append(location.getTime());
        sb.append("\nerror code : ");
        sb.append(location.getLocType());
        sb.append("\nlatitude : ");
        sb.append(location.getLatitude());
        sb.append("\nlongitude : ");
        sb.append(location.getLongitude());
        sb.append("\nradius : ");
        sb.append(location.getRadius());
        if (location.getLocType() == BDLocation.TypeGpsLocation) {
            sb.append("\nspeed : ");
            sb.append(location.getSpeed());
            sb.append("\nsatellite : ");
            sb.append(location.getSatelliteNumber());
        } else if (location.getLocType() == BDLocation.TypeNetWorkLocation) {
            sb.append("\naddr : ");
            sb.append(location.getAddrStr());
        }
        Log.i("loc info:",sb.toString());

       // Toast.makeText(context,location.getLatitude() + "," +location.getLongitude(),Toast.LENGTH_SHORT ).show();

        String locationString = "&location=" + location.getLatitude() + ","
                + location.getLongitude();
        String keyString = "&key=j8z7NerNBo8I19ABkXBZsgef";
        String questURL = "http://api.map.baidu.com/geocoder?output=json" + locationString + keyString;
        new ReadJSONFeedTask(activity).execute(questURL);
    }
}
