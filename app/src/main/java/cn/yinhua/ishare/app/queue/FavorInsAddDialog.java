package cn.yinhua.ishare.app.queue;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.widget.Toast;


import cn.yinhua.ishare.app.R;

/**
 * Created by Edward on 5/7/15.
 */
public class FavorInsAddDialog extends DialogFragment {

    private Context context;
    private String insInfoString;

    public void setContext(Context context) {
        this.context = context;
    }

    public void setInsInfoString(String insInfoString) {
        this.insInfoString = insInfoString;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getResources().getString(R.string.dialog_ins_favorites_add_des))
        .setPositiveButton(R.string.dialog_ins_favorites_add_yes,new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
                String prefInsKey = getResources().getString(R.string.preference_favorites_ins_set);

                String favorites = sp.getString(prefInsKey, null);

                if(favorites == null){

                    sp.edit().putString(prefInsKey, insInfoString).commit();
                }else{
                    if(favorites.contains(insInfoString)){
                        Toast.makeText(context,R.string.dialog_ins_favorites_add_repeat ,Toast.LENGTH_LONG).show();
                        return;
                    }else{
                        String[] infoItems = insInfoString.split("\\$");
                        String insATNAdd = infoItems[0] + "$" + infoItems[1] + "$" + infoItems[2];
                        if(favorites.contains(insATNAdd)){
                            String[] favoritesItems = favorites.split("\\|");
                            String updateFavorites = "";
                            boolean isFirstTime = true;

                            for(String item : favoritesItems){
                                if(!isFirstTime){
                                    if(item.contains(insATNAdd))updateFavorites = updateFavorites + "|"
                                            + insInfoString;
                                    else  updateFavorites = updateFavorites + "|" + item;
                                }else{

                                    if(item.contains(insATNAdd))updateFavorites =  insInfoString;
                                    else  updateFavorites =  item;
                                }


                                isFirstTime = false;
                            }

                            sp.edit().putString(prefInsKey,updateFavorites).commit();


                        }else {
                            favorites = favorites + "|" + insInfoString;
                            sp.edit().putString(prefInsKey,favorites).commit();
                        }


                    }


                }

                Toast.makeText(context,R.string.dialog_ins_favorites_add_successful ,Toast.LENGTH_LONG).show();
            }
        }).setNegativeButton(R.string.dialog_ins_favorites_add_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        return builder.create();

    }
}
