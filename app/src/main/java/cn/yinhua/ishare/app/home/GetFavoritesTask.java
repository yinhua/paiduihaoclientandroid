package cn.yinhua.ishare.app.home;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.yinhua.ishare.app.MainActivity;
import cn.yinhua.ishare.app.R;
import cn.yinhua.ishare.app.queue.QueueActivity;

/**
 * Created by Edward on 6/11/15.
 */
public class GetFavoritesTask extends AsyncTask {

    private MainActivity activity ;
    private FrameLayout frameLayout;
    private LayoutInflater inflater;
    private String ac;

    public void setAc(String ac) {
        this.ac = ac;
    }

    public void setActivity(MainActivity activity) {
        this.activity = activity;
    }

    public void setFrameLayout(FrameLayout frameLayout) {
        this.frameLayout = frameLayout;
    }

    public void setInflater(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    private void addInst(String insStr,ArrayList<Institution> data ){
        //System.out.println("++++++++++++ add Inst :" +insStr);
        String[] infoItems = insStr.split("\\$");

        String insAT = infoItems[0];
        String name = infoItems[1];
        String address =  infoItems[2];
        String subs = infoItems[3];

        String[] insATArr = insAT.split("-");

        Institution institution = new Institution();
        institution.setAc(insATArr[0]);
        institution.setIt(insATArr[1]);
        institution.setIn(name);
        institution.setIa(address);
        institution.setSubs(subs);

        data.add(institution);
    }

    @Override
    protected Object doInBackground(Object[] params) {


        ArrayList<Institution> data  = new ArrayList<Institution>();


        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
        String prefInsKey = activity.getResources().getString(R.string.preference_favorites_ins_set);

        String favorites = sp.getString(prefInsKey, null);
        if(favorites == null || favorites.length() ==0){
            // favorites is empty, get some ins from sever.

            String AC= ac;
            // activity.getLocationTextView().getTag().toString();
            String getBaseUrl = activity.getString(R.string.institution_get_url);

            String getUrl = getBaseUrl + "?areaTypeCode=" + AC + "&filter=fourGet";

            String result="";
            try{
                HttpGet httpGet=new HttpGet(getUrl);
                //取得HTTP response
                HttpResponse response=new DefaultHttpClient().execute(httpGet);
                //若状态码为200
                if(response.getStatusLine().getStatusCode()==200){
                    //取出应答字符串
                    HttpEntity entity=response.getEntity();
                    result= EntityUtils.toString(entity, HTTP.UTF_8);
                }
            }catch (ClientProtocolException e) {
                e.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();

            }

            if(result.length() > 2){// == 2 ,"[]"

                String[] instArr = result.split("\\},");
                for(int len = 0; len<instArr.length ; len++){
                    String instStr = instArr[len].replace("[","").replace("]","").replace("{","").replace("}","");
                    HashMap<String,String> instMap = new HashMap<String, String>();
                    String[] items = instStr.split(",");
                    for(int iteLen = 0; iteLen <items.length; iteLen++){
                        String item = items[iteLen].replace("\"", "");

                        String entity[] = item.split(":");

                        if(entity.length == 2)instMap.put(entity[0],entity[1]);

                    }
                    String insString = instMap.get("insAT") + "$" + instMap.get("name") + "$" +
                            instMap.get("address") + "$" + instMap.get("subs");
                    addInst(insString,data);
                }

            }


        }else {
            String[] favoritesItems = favorites.split("\\|");

           // System.out.println("-------------length is " + favorites.length() + ", content is " +favorites);

            if(favoritesItems.length > 4){  // favorites are more than 4, then show latest 4 .
                for(int i = favoritesItems.length; i > favoritesItems.length - 4; i--){
                    String item =  favoritesItems[i - 1];
                    addInst(item,data);
                }
            }else {
                for(String item : favoritesItems){
                    addInst(item,data);
                }
            }

        }


        return data;
    }

    /**
     *  getting area code from activity must be processed in method onPostExecute,
     *  because  this method is run in UI thread  which can change UI.
     *  method doInBackground is running synchronously with UI thread.
     * @param o
     */
    @Override
    protected void onPostExecute(Object o) {

        ArrayList<Institution> data = (ArrayList<Institution>)o;


        final ListView listView = (ListView)inflater.inflate(R.layout.favorites_list_view, frameLayout, false);

        listView.setAdapter(new InstListAdapter(activity, data));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Object o = listView.getItemAtPosition(position);
                Institution newsData = (Institution) o;
                //System.out.println("+++++++ data 5 :" + newsData.getAc()+ "," +  newsData.getIt() + "," +  newsData.getIa()) ;
                String insATND = newsData.getAc()+"-"+newsData.getIt()
                        + "$" + newsData.getIn()
                        + "$" + newsData.getIa()
                        + "$" + newsData.getSubs()
                        + "$" +  newsData.getIr();
                Intent intent = new Intent(activity, QueueActivity.class);
                intent.putExtra("insATND", insATND);
               // System.out.println("+++++++++" + insATND);
                activity.startActivity(intent);

                // Toast.makeText(getActivity(), "Selected :" + " " + newsData.getIn(), Toast.LENGTH_LONG).show();
            }

        });

        frameLayout.removeAllViews();
        frameLayout.addView(listView);

    }




}
