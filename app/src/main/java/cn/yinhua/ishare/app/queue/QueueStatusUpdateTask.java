package cn.yinhua.ishare.app.queue;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import cn.yinhua.ishare.app.R;

/**
 * Created by Edward on 5/6/15.
 */
public class QueueStatusUpdateTask extends AsyncTask<String,Void,String> {

    private WeakReference<EditText> queueStatusReference ;
    private WeakReference<TextView> queueStatusUpdateByReference;
    private WeakReference<Button> queueStatusUpdateButtonReference;
    private Context context;


    QueueStatusUpdateTask(EditText queueStatus, TextView queueStatusUpdateBy,Button queueStatusUpdate, Context context){

        queueStatusReference = new WeakReference<EditText>(queueStatus);
        queueStatusUpdateByReference = new WeakReference<TextView>(queueStatusUpdateBy);
        queueStatusUpdateButtonReference = new WeakReference<Button>(queueStatusUpdate);
        this.context = context;
    }

    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p/>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected String doInBackground(String... params) {

        String atn = params[0];

        String number= "";
        if(queueStatusReference.get() ==null){
            return null;
        }else {
            number = queueStatusReference.get().getText().toString();
        }
        String baseUrl = context.getResources().getString(R.string.queue_status_get_url);
       // String getUrl = getBaseUrl + "?insATN=" +atns ;

        String result= "";

        TelephonyManager tMgr =(TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        String mPhoneNumber = tMgr.getLine1Number();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        String author = sp.getString(context.getResources().getString(R.string.preference_user_login_id), "null");

        if(mPhoneNumber !=  null) author = author + "$" + mPhoneNumber;
        else  author = author + "$" + "null";

        try{

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(baseUrl);

            JSONObject holder = new JSONObject();

            holder.put("insATN", atn);

            holder.put("commitUser",author);

            holder.put("number",number);


            StringEntity se = new StringEntity(holder.toString(), HTTP.UTF_8);
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setHeader("Accept", "application/json");
            post.setHeader("Content-type", "application/json");
            post.setEntity(se);
            HttpResponse response = client.execute(post);

            if(response.getStatusLine().getStatusCode()==200){
                //取出应答字符串
                HttpEntity entity=response.getEntity();
                result= EntityUtils.toString(entity, HTTP.UTF_8);

            }

        }catch (JSONException e){
            Log.e("update status json e: ",e.getMessage());
        } catch (ClientProtocolException e) {
            Log.e("update status client protocol e: ",e.getMessage());
        } catch (UnsupportedEncodingException e) {
            Log.e("update status encoding e: ",e.getMessage());
        } catch (IOException e) {
            Log.e("update status io e: ",e.getMessage());
        }



        return result;
    }


    /**
     * <p>Runs on the UI thread after {@link #doInBackground}. The
     * specified result is the value returned by {@link #doInBackground}.</p>
     * <p/>
     * <p>This method won't be invoked if the task was cancelled.</p>
     *
     * @param s The result of the operation computed by {@link #doInBackground}.
     * @see #onPreExecute
     * @see #doInBackground
     * @see #onCancelled(Object)
     */
    @Override
    protected void onPostExecute(String s) {


        if(queueStatusUpdateButtonReference.get() != null) queueStatusUpdateButtonReference.get().setText(R.string.queue_status_update_button);
        if(queueStatusUpdateByReference.get() != null){

            Map<String,String> map  = new HashMap<String,String>();
            String[] queueStatus = s.replace("{","").replace("}","").split(",");
            for(String str: queueStatus){
                String[] enti =  str.replace("\"","").split(":");
                if(enti.length == 2){
                    map.put(enti[0], enti[1]);
                }
            }

            String lastupdateuser = "未知";

            if(map.get("lastUpdateUser") != null && !map.get("lastUpdateUser").equals("null")){

                if(map.get("lastUpdateUser").startsWith("null")){

                    if(map.get("lastUpdateUser").split("\\$").length == 1)lastupdateuser = "未知";
                    else{
                        String num = map.get("lastUpdateUser").split("\\$")[1];
                        if(num.equals("null"))lastupdateuser = "未知";
                        else lastupdateuser = "*" + num.substring(num.length()-4,num.length());
                    }

                }else{
                    lastupdateuser = map.get("lastUpdateUser").split("\\$")[0];
                }
            }

            queueStatusUpdateByReference.get().setText("提供者:"+ lastupdateuser);

        }

    }
}
