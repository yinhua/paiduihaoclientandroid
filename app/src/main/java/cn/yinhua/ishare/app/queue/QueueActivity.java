package cn.yinhua.ishare.app.queue;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.internal.widget.ListPopupWindow;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import cn.yinhua.ishare.app.R;

public class QueueActivity extends ActionBarActivity {

    private ListView queueStatusLV;

    private Spinner subSpinner;

    private  Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        setContentView(R.layout.activity_queue);
        LinearLayout queueLinerLayOut  = (LinearLayout)findViewById(R.id.inst_q_des_llayout);
        ImageView typeImg = (ImageView)queueLinerLayOut.findViewById(R.id.q_instTypesImg);
        LinearLayout q_instInfo = (LinearLayout)queueLinerLayOut.findViewById(R.id.q_instInfo);
        TextView insName = (TextView)q_instInfo.findViewById(R.id.q_instNameTxt);
        TextView insAddress = (TextView)q_instInfo.findViewById(R.id.q_instAdsTxt);


        Intent intent = getIntent();
        final String insInfo = intent.getExtras().getString("insATND");


        String[] insInfArr = insInfo.split("\\$");

        String insAT = insInfArr[0];
        String name = insInfArr[1];
        String address =  insInfArr[2];
        String subs = insInfArr[3];

        queueLinerLayOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FavorInsAddDialog favorDialog = new FavorInsAddDialog();
                favorDialog.setContext(context);
                favorDialog.setInsInfoString(insInfo);
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                ft.add(favorDialog, "");
                ft.commitAllowingStateLoss();
            }
        });

        String insType = insAT.split("-")[1];

        if(insType.equals("1")){
            // hospital
            typeImg.setImageResource(R.drawable.hospital);
        }else if(insType.equals("3")){
            //bank
            typeImg.setImageResource(R.drawable.bank);
        }else if(insType.equals("2")){
            //restaurant
            typeImg.setImageResource(R.drawable.restaurant);
        }else {
            // others
            typeImg.setImageResource(R.drawable.others);
        }

        insName.setText(name);
        insAddress.setText(address);

        LinearLayout queueStatusLayOut  = (LinearLayout)findViewById(R.id.q_status_linearLayout);
        final ListView queueStatusListView = (ListView)queueStatusLayOut.findViewById(R.id.q_status_listView);
        queueStatusLV = queueStatusListView;

        View footerView = ((LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.queue_status_update, null, false);
        queueStatusListView.addFooterView(footerView);
        queueStatusListView.setTextFilterEnabled(true);

        ArrayList<String> atns = new ArrayList<String>();
        String atn =insAT + "-"+ name ;
        boolean hasSubs = false;
        if(!subs.equals("null")){
            hasSubs = true;
            String[] subArr = subs.split(";");
            for(String sub : subArr){
                atns.add(atn+ "-" +sub);
            }
        }else atns.add(atn);

        QueueStatusListAdapter queueStatusAda  = new QueueStatusListAdapter(getApplicationContext(),atns ,hasSubs, queueStatusListView);


        queueStatusListView.setAdapter(queueStatusAda);


        queueStatusListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position == queueStatusListView.getAdapter().getCount() -1){

                    String insATND = insInfo;
                    Intent intent = new Intent(context, SubsActivity.class);
                    intent.putExtra("insATND", insATND);
                    startActivityForResult(intent,getResources().getInteger(R.integer.request_code_subs_add));
                }
            }
        });

        LinearLayout queueMineInfoLinearLayout  = (LinearLayout)findViewById(R.id.q_mine_info_linearLayout);

        LinearLayout queueMineStatusll = (LinearLayout)queueMineInfoLinearLayout.findViewById(R.id.q_mine_status_linearLayout);
        final Spinner queueSubSpinner = (Spinner)queueMineStatusll.findViewById(R.id.queue_sub_spinner);
        subSpinner = queueSubSpinner;

        String[] subsSpinnerArray;
        if(hasSubs){
            String[] subArr = subs.split(";");
            subsSpinnerArray = new String[subArr.length];
            int index = 0;
            for(String sub : subArr){
                subsSpinnerArray[index] = sub;
                index++;
            }
        }
        else{
            subsSpinnerArray = new String[1];
            subsSpinnerArray[0]="默认";
        }

        ArrayList spAL = new ArrayList(subsSpinnerArray.length);
        for(int i = 0; i< subsSpinnerArray.length ;i++){
            spAL.add(i,subsSpinnerArray[i]);
        }
        ArrayAdapter subSpinnerAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,spAL);
        subSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        queueSubSpinner.setAdapter(subSpinnerAdapter);


        final EditText numberMine = (EditText)queueMineStatusll.findViewById(R.id.q_status_mine);
        LinearLayout queueMineTimeDiffll = (LinearLayout)queueMineInfoLinearLayout.findViewById(R.id.q_mine_time_diff_linearLayout);

        final TextView numberDiff = (TextView)queueMineTimeDiffll.findViewById(R.id.queue_difference_number);

        TextView timeLeft = (TextView)queueMineTimeDiffll.findViewById(R.id.queue_time_left);

        numberMine.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(s.toString() != null && s.toString().length() >=1){

                    HeaderViewListAdapter hvla = (HeaderViewListAdapter)queueStatusLV.getAdapter();
                    QueueStatusListAdapter qsla = (QueueStatusListAdapter)hvla.getWrappedAdapter();
                    HashMap<String,String> queueStatusMap = qsla.getQueueStatusMap();

                    String selItem = (String)queueSubSpinner.getSelectedItem();

                    Set<Map.Entry<String,String>> st =queueStatusMap.entrySet();
                    Iterator<Map.Entry<String,String>> ite = st.iterator();

                    String currentNumber = null ;

                    if(queueStatusMap.size() == 1){
                        while(ite.hasNext()){
                            Map.Entry<String,String> entry = ite.next();
                            currentNumber = entry.getValue();
                            break;
                        }
                    }else{
                        while(ite.hasNext()){
                            Map.Entry<String,String> entry = ite.next();
                            if(entry.getKey().contains("-"+ selItem)){
                               // System.out.println(entry.getKey()+ ","+entry.getValue());
                                currentNumber = entry.getValue();
                                break;
                            }

                        }
                    }

                    if(currentNumber !=  null){

                        currentNumber = currentNumber.toLowerCase().replaceAll("[a-z]", "");
                       // System.out.println("current num:" +currentNumber);
                        String myNumStr = s.toString().toLowerCase().replaceAll("[a-z]", "");

                        if(currentNumber.length() ==0 || myNumStr.length() ==0){
                            numberDiff.setText("0");
                        }else{

                            int diff = Integer.parseInt(myNumStr) - Integer.parseInt(currentNumber);
                            //System.out.println(diff);
                            if(diff > 0)numberDiff.setText(diff + "");
                            else numberDiff.setText("0");
                        }

                    }
                }


               // qsla.getItemViewType(1)''

            }
        });

        queueSubSpinner.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String s = numberMine.getText().toString();

                if(s.toString() != null && s.toString().length() >=1){

                    HeaderViewListAdapter hvla = (HeaderViewListAdapter)queueStatusLV.getAdapter();
                    QueueStatusListAdapter qsla = (QueueStatusListAdapter)hvla.getWrappedAdapter();
                    HashMap<String,String> queueStatusMap = qsla.getQueueStatusMap();

                    String selItem = (String)queueSubSpinner.getSelectedItem();

                    Set<Map.Entry<String,String>> st =queueStatusMap.entrySet();
                    Iterator<Map.Entry<String,String>> ite = st.iterator();

                    String currentNumber = null ;

                    if(queueStatusMap.size() == 1){
                        while(ite.hasNext()){
                            Map.Entry<String,String> entry = ite.next();
                            currentNumber = entry.getValue();
                            break;
                        }
                    }else{
                        while(ite.hasNext()){
                            Map.Entry<String,String> entry = ite.next();
                            if(entry.getKey().contains("-"+ selItem)){
                               // System.out.println(entry.getKey()+ ","+entry.getValue());
                                currentNumber = entry.getValue();
                                break;
                            }

                        }
                    }

                    if(currentNumber !=  null){

                        currentNumber = currentNumber.toLowerCase().replaceAll("[a-z]", "");
                        //System.out.println("current num:" +currentNumber);
                        String myNumStr = s.toString().toLowerCase().replaceAll("[a-z]", "");

                        if(currentNumber.length() ==0 || myNumStr.length() ==0){
                            numberDiff.setText("0");
                        }else{

                            int diff = Integer.parseInt(myNumStr) - Integer.parseInt(currentNumber);
                           // System.out.println(diff);
                            if(diff > 0)numberDiff.setText(diff + "");
                            else numberDiff.setText("0");
                        }

                    }
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        } );

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_queue, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Dispatch incoming result to the correct fragment.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == getResources().getInteger(R.integer.request_code_subs_add)){
            if(data != null){

                final String instInfo = data.getExtras().getString("insATND");

                String[] insInfArr = instInfo.split("\\$");

                String insAT = insInfArr[0];
                String name = insInfArr[1];
                String address =  insInfArr[2];
                String subs = insInfArr[3];

                ArrayList<String> atns = new ArrayList<String>();
                String atn =insAT + "-"+ name ;
                boolean hasSubs = false;
                if(!subs.equals("null")){
                    hasSubs = true;
                    String[] subArr = subs.split(";");
                    for(String sub : subArr){
                        atns.add(atn+ "-" +sub);
                    }
                }else atns.add(atn);

                QueueStatusListAdapter queueStatusAda  = new QueueStatusListAdapter(getApplicationContext(),atns ,hasSubs, queueStatusLV);
                queueStatusLV.setAdapter(queueStatusAda);
                /* HeaderViewListAdapter hvla = (HeaderViewListAdapter)queueStatusLV.getAdapter();
                QueueStatusListAdapter qsla = (QueueStatusListAdapter)hvla.getWrappedAdapter();


                qsla.setAtns(atns);
                qsla.setHasSubs(hasSubs);
                qsla.notifyDataSetChanged();*/

//                queueStatusLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                        if(position == queueStatusLV.getAdapter().getCount() -1){
//
//                            String insATND = instInfo;
//                            Intent intent = new Intent(context, SubsActivity.class);
//                            intent.putExtra("insATND", insATND);
//                            startActivityForResult(intent,getResources().getInteger(R.integer.request_code_subs_add));
//                        }
//                    }
//                });

                ArrayAdapter  spinnerAdapter = (ArrayAdapter)subSpinner.getAdapter();
                spinnerAdapter.clear();
                if(!hasSubs){
                    spinnerAdapter.add("默认");
                }else{
                    String[] subArr = subs.split(";");
                    for(String sub : subArr){
                        spinnerAdapter.add(sub);
                    }

                }

                spinnerAdapter.notifyDataSetChanged();
            }
        }
    }
}
