package cn.yinhua.ishare.app;


import android.content.Intent;
import android.content.SharedPreferences;

import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;

import android.support.v4.app.FragmentManager;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.support.v4.widget.DrawerLayout;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import cn.yinhua.ishare.app.Location.LocationListener;
import cn.yinhua.ishare.app.Location.ReadJSONFeedTask;
import cn.yinhua.ishare.app.home.GetFavoritesTask;
import cn.yinhua.ishare.app.home.HomeMainFragment;
import cn.yinhua.ishare.app.home.InstTypeAdapter;
import cn.yinhua.ishare.app.home.InstitutionType;
import cn.yinhua.ishare.app.home.LoadingFragment;
import cn.yinhua.ishare.app.home.LocationAffirmDialogFragment;

import cn.yinhua.ishare.app.home.MyMainFragment;
import cn.yinhua.ishare.app.queue.QueueActivity;
import cn.yinhua.ishare.institution.InstituionCrActivity;
import cn.yinhua.ishare.location.MainLocActivity;

import cn.yinhua.ishare.search.SearchActivity;
import it.sephiroth.android.library.widget.HListView;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks ,ActionBar.TabListener,
                    ReadJSONFeedTask.LocationCallbacks{

    private LocationClient mLocationClient;
    private LocationListener myListener;
    private TextView locationTextView;
    private Map<String,String> cities ;
    private boolean isLocated;
    private boolean isMyTab;



    public TextView getLocationTextView() {
        return locationTextView;
    }

    @Override
    public void onLocationGotCallBack(String city ){
            Log.i("Already got the city: ", city);

            if(city!= null && city.length()>0){

                if(city.endsWith("市")){
                    city = city.substring(0,city.length()-1);
                }
                Log.i("cities length","" + cities.size());
                if(cities.containsKey(city) && !city.equals(locationTextView.getText())){
                    //LocationAffirmDialogFragment laf = new LocationAffirmDialogFragment(city, cities.get(city), locationTextView);
                    LocationAffirmDialogFragment laf = new LocationAffirmDialogFragment();
                    laf.setCity(city);
                    laf.setPostalCode(cities.get(city));
                    laf.setLocationTextView(locationTextView);
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.add(laf, "");
                    ft.commitAllowingStateLoss();
                }
                isLocated = true;
                if(mLocationClient != null && mLocationClient.isStarted()){
                    if(myListener != null){
                        mLocationClient.unRegisterLocationListener(myListener);
                        myListener = null;
                    }
                    mLocationClient.stop();
                    mLocationClient = null;

                }

            }
        }


//    @Override
//    public void onItemClick(it.sephiroth.android.library.widget.AdapterView<?> adapterView, View view, int i, long l) {
//        Intent intent = new Intent(this,SearchActivity.class);
//        intent.putExtra("cityStr", locationTextView.getTag() + "," +locationTextView.getText() );
//        intent.putExtra("typeSearch","typeSearch");
//        Object o = favoritesInstList.getItemAtPosition(position);
//        startActivityForResult(intent, getResources().getInteger(R.integer.request_code_search));
//
//    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
     //       TextView tv = (TextView)  tab. tab.findViewById(android.R.id.title);
//             tv.setTextColor(getResources().getColor(R.color.lightgreen));

        if(tab.getPosition() == 0){


            if(isMyTab){
                isMyTab = false;


                HomeMainFragment homeMainFragment = new HomeMainFragment();
                homeMainFragment.setMainActivity(this);
                homeMainFragment.setAc(getLocationTextView().getTag().toString());
                FragmentManager fragmentManager = this.getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.mainPage, homeMainFragment).commit();
            }

        }else{

            isMyTab = true;
            LinearLayout  mainPage  = (LinearLayout)findViewById(R.id.mainPage);
            mainPage.removeAllViews();

            MyMainFragment myMainF = new MyMainFragment();
            myMainF.setActivity(this);
            FragmentManager fragmentManager = this.getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.mainPage, myMainF).commit();


        }
       // System.out.println( "tab selected position :"+tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        //mNavigationDrawerFragment = (NavigationDrawerFragment)getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
      //  mNavigationDrawerFragment.setUp( R.id.navigation_drawer,(DrawerLayout) findViewById(R.id.drawer_layout));
        initHomePage(getLayoutInflater());

    }


    private LinearLayout initHomePage( LayoutInflater layoutInflater){

        LinearLayout  mainPage  = (LinearLayout)findViewById(R.id.mainPage);


        LoadingFragment lft = new LoadingFragment();
        lft.setContext(this);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.favoritesFrame,lft).commit();


        LinearLayout instTypeLayout = (LinearLayout)mainPage.findViewById(R.id.instTypeLayout);
        final HListView instituteTypesList = (HListView)instTypeLayout.findViewById(R.id.instituteTypes);


        instituteTypesList.setAdapter(new InstTypeAdapter(getApplicationContext(),getInstType()));
        instituteTypesList.setOnItemClickListener(new it.sephiroth.android.library.widget.AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(it.sephiroth.android.library.widget.AdapterView<?> adapterView, View view, int i, long l) {
                Object o = instituteTypesList.getItemAtPosition(i);
                InstitutionType inst = (InstitutionType)o;

                Intent intent = new Intent(getApplicationContext(),SearchActivity.class);
                intent.putExtra("cityStr", getLocationTextView().getTag() + "," + getLocationTextView().getText() );
                intent.putExtra("typeSearch","typeSearch");
                intent.putExtra("searchKey",inst.getItd());
                startActivityForResult(intent, getResources().getInteger(R.integer.request_code_search));

            }
        } );



        if(!isLocated){
            mLocationClient = new LocationClient(this.getApplicationContext()); // 声明LocationClient类
            myListener = new LocationListener(this);
            mLocationClient.registerLocationListener(myListener); // 注册监听函数

            LocationClientOption option = new LocationClientOption();
            option.setLocationMode(LocationClientOption.LocationMode.Battery_Saving);// 设置定位模式
            option.setCoorType("bd09ll");// 返回的定位结果是百度经纬度，默认值gcj02
            int span = 10000;

            option.setScanSpan(span);// 设置发起定位请求的间隔时间为5000ms
            option.setIsNeedAddress(false);
            mLocationClient.setLocOption(option);
            mLocationClient.start();
        }


        //init cities
        cities = new HashMap<String, String>();
        String[] otherCities = getResources().getStringArray(R.array.cities);
        String[] municipalities = getResources().getStringArray(R.array.municipality);
        for(String mp : municipalities){
            cities.put(mp.split(",")[1], mp.split(",")[0]);
        }
        for(String oc : otherCities){
            cities.put(oc.split(",")[1], oc.split(",")[0]);
        }

        return  mainPage;
    }


    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments

        /*
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position))
                .commit();*/
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 0:
                mTitle = getString(R.string.title_section1);
                break;
            case 1:
                mTitle = getString(R.string.title_section2);
                break;
            case 2:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        final MenuItem item = menu.findItem(R.id.location_choose);
        View locationView = MenuItemCompat.getActionView(item);
        locationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onOptionsItemSelected(item);
            }
        });
        locationTextView = (TextView)locationView.findViewById(R.id.ab_loc_txt);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        String prefCity = sp.getString(getResources().getString(R.string.constant_preference_string_city), "");
        if(prefCity.length() > 0){
            locationTextView.setText(prefCity.split(",")[1]);
            locationTextView.setTag(prefCity.split(",")[0]);
            Log.i("using preference city:",locationTextView.getText().toString());
        }else {
            locationTextView.setTag("021");
            Log.i("using hard code city :",locationTextView.getText().toString());
        }

       // sp.edit().clear().commit();
        //System.out.println("favorites ins:" + sp.getString(getResources().getString(R.string.preference_favorites_ins_set),"no favorites"));

        addTabs();
        /*
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            if(onCreateOptionsMenuCalledTime == 1){
                addTabs();
            }else restoreActionBar();
            return true;
        }*/


        LinearLayout  mainPage  = (LinearLayout)findViewById(R.id.mainPage);
        GetFavoritesTask gft = new GetFavoritesTask();
        gft.setActivity(this);
        gft.setFrameLayout((FrameLayout)mainPage.findViewById(R.id.favoritesFrame));
        gft.setInflater(getLayoutInflater());
        gft.setAc(getLocationTextView().getTag().toString());
        gft.execute();

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.location_choose) {

            Intent intent = new Intent(this,MainLocActivity.class);
            startActivityForResult(intent,getResources().getInteger(R.integer.request_code_location_choose));
            return true;
        } else if(id == R.id.instituteSearch){
            //onSearchRequested();
            Intent intent = new Intent(this,SearchActivity.class);
            intent.putExtra("cityStr", locationTextView.getTag() + "," +locationTextView.getText() );
            startActivityForResult(intent, getResources().getInteger(R.integer.request_code_search));
            return true;
        }else if(id == R.id.instituteAdd){

            Intent intent = new Intent(this, InstituionCrActivity.class);
            intent.putExtra("cityStr", locationTextView.getTag() + "," +locationTextView.getText() );
            startActivityForResult(intent, getResources().getInteger(R.integer.request_code_institute_add));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        System.out.println("request code:" + requestCode);

        if (requestCode == getResources().getInteger(R.integer.request_code_location_choose)){// location change
           if(data != null){
               String city =  (String)data.getExtras().get("cityStr");
               if(city.indexOf(',') > 0){
                   locationTextView.setText(city.split(",")[1]);
                   locationTextView.setTag(city.split(",")[0]);
                   SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
                   String prefCityKey = getResources().getString(R.string.constant_preference_string_city);
                   sp.edit().putString(prefCityKey,city).commit();
               }
           }
        }else if(requestCode == getResources().getInteger(R.integer.request_code_institute_add)){
                if(data !=null){
                    String insATND =(String)data.getExtras().get("insATND");
                    if(insATND.indexOf('$') > 0){
                        Intent intent = new Intent(this, QueueActivity.class);
                        intent.putExtra("insATND", insATND);
                        startActivity(intent);

                    }
                }
        }else if(requestCode == getResources().getInteger(R.integer.request_code_search) ||
                resultCode == 666){
            if(data != null){
                String searchRes = (String)data.getExtras().get("searchResult");
                //System.out.println("search result: " + searchRes);
                String[] resSet = searchRes.split(",");
                HashMap<String,String> instMap = new HashMap<String, String>();
                for(int co = 0; co<resSet.length ;co ++){
                    String insInfo = resSet[co].replace("\"","");
                    String entity[] = insInfo.split(":");

                    instMap.put(entity[0],entity[1]);
                }

                String insATND = instMap.get("insAT")+ "$" +instMap.get("name")
                        + "$" + instMap.get("address")
                        + "$" + instMap.get("subs")
                        + "$" + instMap.get("author");
                Intent intent = new Intent(this, QueueActivity.class);
                intent.putExtra("insATND", insATND);
                startActivity(intent);
               // System.out.println("RC:" + requestCode + ", start queue activity.");
            }
        }
    }



    //add tabs on to the action bar.
    private void addTabs(){
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        if(actionBar.getTabCount() == 0){
            ActionBar.Tab tab = actionBar.newTab().setText(getString(R.string.tab_welcome)).setTabListener(this);
            actionBar.addTab(tab);
            tab = actionBar.newTab().setText(getString(R.string.tab_me)).setTabListener(this);
            actionBar.addTab(tab);
        }
    }

    public   static List<InstitutionType> getInstType(){
        ArrayList<InstitutionType> data = new ArrayList<InstitutionType>();

        InstitutionType it = new InstitutionType();
        it.setIt("1");
        it.setItd("医院");
        data.add(it);

        InstitutionType itb = new InstitutionType();
        itb.setItd("银行");
        itb.setIt("3");
        data.add(itb);

        InstitutionType itr = new InstitutionType();
        itr.setItd("餐馆");
        itr.setIt("2");
        data.add(itr);

        InstitutionType ito = new InstitutionType();
        ito.setIt("4");
        ito.setItd("其他");
        data.add(ito);

        return data;

    }

    public boolean isLocated() {
        return isLocated;
    }

    public void setIsLocated(boolean isLocated) {
        this.isLocated = isLocated;
    }

    public Map<String, String> getCities() {
        return cities;
    }

    public void setCities(Map<String, String> cities) {
        this.cities = cities;
    }
}
