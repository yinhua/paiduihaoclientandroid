package cn.yinhua.ishare.app.queue;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import cn.yinhua.ishare.app.R;

public class SubsActivity extends ActionBarActivity implements SubsUpdateDialog.FinishActivity {


    @Override
    public void doFinish(String insATN) {
        Intent intent = new Intent();
        intent.putExtra("insATND",insATN);

        setResult(RESULT_OK,intent);
        finish();
    }

    private String insInformation ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subs);
        LinearLayout subsInstInfo  = (LinearLayout)findViewById(R.id.subs_inst_info_ll);

        ImageView typeImg = (ImageView)subsInstInfo.findViewById(R.id.subs_instTypesImg);
        LinearLayout subs_instInfo = (LinearLayout)subsInstInfo.findViewById(R.id.subs_instInfo);
        TextView insName = (TextView)subs_instInfo.findViewById(R.id.subs_instNameTxt);
        TextView insAddress = (TextView)subs_instInfo.findViewById(R.id.subs_instAdsTxt);


        Intent intent = getIntent();
        final String insInfo = intent.getExtras().getString("insATND");
        insInformation = insInfo;

        String[] insInfArr = insInfo.split("\\$");

        String insAT = insInfArr[0];
        String name = insInfArr[1];
        String address =  insInfArr[2];
        String subs = insInfArr[3];

        String insType = insAT.split("-")[1];

        if(insType.equals("1")){
            // hospital
            typeImg.setImageResource(R.drawable.hospital);
        }else if(insType.equals("3")){
            //bank
            typeImg.setImageResource(R.drawable.bank);
        }else if(insType.equals("2")){
            //restaurant
            typeImg.setImageResource(R.drawable.restaurant);
        }else {
            // others
            typeImg.setImageResource(R.drawable.others);
        }

        insName.setText(name);
        insAddress.setText(address);

        LinearLayout subsEditll  = (LinearLayout)findViewById(R.id.subs_edit_text_ll);
        LinearLayout subsCorell = (LinearLayout)subsEditll.findViewById(R.id.subs_edit_core_ll);
        final EditText editText = (EditText)subsCorell.findViewById(R.id.subs_edit_text);
        if(!subs.equals("null")){
            subs = subs.replace(";",",");
            editText.setText(subs +",");
            int pos = editText.getText().length();
            editText.setSelection(pos);
        }
        final Button button = (Button)subsEditll.findViewById(R.id.subs_add_submit_but);
        final SubsUpdateDialog.FinishActivity finishActivity = this;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String subs = editText.getText().toString();
                if(subs == null || subs.length() ==0){
                    Toast.makeText(getBaseContext(),R.string.subs_add_check_null_msg ,Toast.LENGTH_LONG).show();
                    return;
                }

                SubsUpdateDialog  subsUpdateDialog = new SubsUpdateDialog();
                subsUpdateDialog.setButton(button);
                subsUpdateDialog.setSubs(subs);
                subsUpdateDialog.setInstInfo(insInfo);
                subsUpdateDialog.setFinishActivity(finishActivity);
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                ft.add(subsUpdateDialog, "");
                ft.commitAllowingStateLoss();

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_subs, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if(id == android.R.id.home ){

            Intent intent = new Intent();
            intent.putExtra("insATND",insInformation);
            setResult(RESULT_CANCELED,intent);
            finish();
            return  true;
        }

        return super.onOptionsItemSelected(item);
    }
}
