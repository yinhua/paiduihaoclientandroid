package cn.yinhua.ishare.app.Location;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Edward on 8/14/14.
 */
public class ReadJSONFeedTask extends AsyncTask<String, Void, String> {

    StringBuilder stringBuilder = new StringBuilder();
    Activity activity ;

    public ReadJSONFeedTask(Activity activity){
        this.activity = activity;
    }

    @Override
    protected String doInBackground(String... strings) {
        return readJSONFeed(strings[0]);
    }

    @Override
    protected void onPostExecute(String result) {
        String strItem;

        try {

            JSONObject jsonObject = new JSONObject(result);
            JSONObject resultObject = jsonObject.getJSONObject("result");
            JSONObject addressComponentObject = resultObject
                    .getJSONObject("addressComponent");
            String city = addressComponentObject.getString("city");
            String district = addressComponentObject.getString("district");

            city = city;
            district = "district:" + district;
            stringBuilder.append(city + ","+ district);
            Log.i("定位信息：" ,stringBuilder.toString());
          //  textView.setText(stringBuilder.toString());
          //  Toast.makeText(context,stringBuilder.toString(),Toast.LENGTH_LONG).show();
            if(! (activity instanceof LocationCallbacks)){
                Log.e(activity.getLocalClassName(),"class hasn't implied interface LocationCallbacks .");
            }
            ((LocationCallbacks) activity).onLocationGotCallBack(city);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }




    public String readJSONFeed(String url) {
        StringBuilder stringBuilder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        HttpResponse response;
        try {
            response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader  reader = new BufferedReader(
                        new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line);
                }
            } else {
                Log.e("JSON", "Failed to download file");
            }
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }


    /**
     * Callbacks interface for getting Baidu location call back .
     */
    public static interface LocationCallbacks {
        /**
         * Called when getting an location.
         */
        void onLocationGotCallBack(String city);
    }

}
