package cn.yinhua.ishare.app.queue;

import android.content.Context;
import android.content.Intent;
import android.support.v7.internal.widget.ListPopupWindow;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;

import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

import cn.yinhua.ishare.app.R;

/**
 * Created by Edward on 4/21/15.
 */
public class QueueStatusListAdapter extends BaseAdapter {


    private List<String> atns;
    private LayoutInflater layoutInflater;
    private Context context;
    private boolean hasSubs;
    private HashMap<String,String> queueStatusMap;
    private HashMap<String, QueueStatusListTask> hasInitMap;
    private ListView listView;

    public QueueStatusListAdapter(Context context, List<String> atns, boolean hasSubs, ListView listView){

        this.atns = atns;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.hasSubs = hasSubs;
        this.queueStatusMap = new HashMap<String, String>();
        this.hasInitMap = new HashMap<String, QueueStatusListTask>(atns.size());
        this.listView = listView;
    }


    public HashMap<String, String> getQueueStatusMap() {
        return queueStatusMap;
    }

    public void setQueueStatusMap(HashMap<String, String> queueStatusMap) {
        this.queueStatusMap = queueStatusMap;
    }

    public void setAtns(List<String> atns) {
        this.atns = atns;
    }

    public void setHasSubs(boolean hasSubs) {
        this.hasSubs = hasSubs;
    }

    /**
     * How many items are in the data set represented by this Adapter.
     *
     * @return Count of items.
     */
    @Override
    public int getCount() {
        return atns.size();
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     *                 data set.
     * @return The data at the specified position.
     */
    @Override
    public Object getItem(int position) {

       return atns.get(position);
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * {@link android.view.LayoutInflater#inflate(int, android.view.ViewGroup, boolean)}
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position    The position of the item within the adapter's data set of the item whose view
     *                    we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     *                    is non-null and of an appropriate type before using. If it is not possible to convert
     *                    this view to display the correct data, this method can create a new view.
     *                    Heterogeneous lists can specify their number of view types, so that this View is
     *                    always of the right type (see {@link #getViewTypeCount()} and
     *                    {@link #getItemViewType(int)}).
     * @param parent      The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

       // if(hasInitMap.containsKey(position) && convertView != null)return  convertView;
        final ViewHolder viewHolder;
        if(convertView == null){//first time to get view

            convertView = layoutInflater.inflate(R.layout.queue_status_list_item, null);
            viewHolder  = new ViewHolder();
            viewHolder.queueStatusDesc = (TextView)convertView.findViewById(R.id.queue_status_desc_tv);
            viewHolder.queueStatus =  (EditText)convertView.findViewById(R.id.queue_status_curr_id);
            viewHolder.queueStatusUpdateBy = (TextView)convertView.findViewById(R.id.queue_status_update_by);
            viewHolder.queueStatusUpdate = (Button)convertView.findViewById(R.id.queue_status_update_butt);
            convertView.setTag(viewHolder);

        }else {
//////           System.out.println("convert view is not null again. des is " + ((ViewHolder)convertView.getTag())
//////           .queueStatusDesc.getText() + " ,position is " + position);
           viewHolder = (ViewHolder)convertView.getTag();
        }

        if(!hasSubs){// there is no subs.
            viewHolder.queueStatusDesc.setText(R.string.queue_status_dec);
            viewHolder.queueStatusUpdate.setText(R.string.queue_status_update_button);

        }else{
            String[] atnsarr = atns.get(position).split("-");
            String sub = atnsarr[atnsarr.length-1];
            String desc = context.getResources().getString(R.string.queue_status_dec);
            viewHolder.queueStatusDesc.setText(sub + desc);

           // System.out.println( position +"!!!!!!!!!!!!!!!!!!!!" + sub + desc + "---" + viewHolder.atn );

            viewHolder.queueStatusUpdate.setText(R.string.queue_status_update_button);
        }

        final String atn =  atns.get(position);
        viewHolder.queueStatus.setTag(atn);
        viewHolder.queueStatusUpdateBy.setTag(atn+ "author");
        viewHolder.queueStatusUpdate.setTag(atn+ "button");

        /* here it not right for updating status. it will be invoked more times for one update task.
         don't know why.
        viewHolder.queueStatus.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String status = s.toString();
                 queueStatusMap.put(atn,status);
                System.out.println("map update: key: " + atn +  ", value: " + status) ;
            }
        });*/

        String[] popList = {"提交"};
        final ListPopupWindow listPopupWindow = new ListPopupWindow(context);
        listPopupWindow.setAdapter(new ArrayAdapter(context, R.layout.queue_status_submit,popList));
        listPopupWindow.setAnchorView(viewHolder.queueStatus);
        listPopupWindow.setPromptPosition(android.support.v7.internal.widget.ListPopupWindow.POSITION_PROMPT_BELOW);

          //listPopupWindow.setModal(true);


            viewHolder.queueStatus.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    listPopupWindow.show();
                    return false;
                }
            });




            listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    listPopupWindow.dismiss();
                    if(viewHolder.queueStatus.getText() == null || viewHolder.queueStatus.length() ==0){
                        Toast.makeText(context,R.string.queue_status_update_number_empty_msg,Toast.LENGTH_LONG).show();
                        return;
                    }
                    viewHolder.queueStatusUpdate.setText(context.getResources().getString(R.string.queue_status_update_submit_process_dec));

                    //System.out.println( " commit :" + holderRef.atn);
                    new QueueStatusUpdateTask(viewHolder.queueStatus,viewHolder.queueStatusUpdateBy,viewHolder.queueStatusUpdate,context).execute(atn);
                }
            });

            viewHolder.listPopupWindow = listPopupWindow;

            viewHolder.queueStatus.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(!hasFocus)
                        listPopupWindow.dismiss();
                        String status = ((EditText)v).getText().toString();
                        queueStatusMap.put(atn,status);
                        System.out.println("map update: key: " + atn +  ", value: " + status) ;
                }
            });


        viewHolder.queueStatusUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.queueStatusUpdate.setText(R.string.queue_status_update_button_running);
                new QueueStatusListTask( context, atn, queueStatusMap,listView)
                        .execute(atn, "from button click.");
            }
        });



     //  if(!hasInitMap.containsKey(position)){
//           System.out.println("set up queue status list task." +" for " + viewHolder.queueStatusDesc.getText());
        if(!hasInitMap.containsKey(atn)){
            QueueStatusListTask task  = new QueueStatusListTask(context, atn, queueStatusMap,listView);
            hasInitMap.put(atn,task);
            task.execute(atn, "from list adapter main.");
        }


     //  }

        return convertView;
    }

    static class ViewHolder {
        TextView queueStatusDesc;
        EditText queueStatus;
        TextView queueStatusUpdateBy;
        Button   queueStatusUpdate;
        ListPopupWindow listPopupWindow;


    }
}
