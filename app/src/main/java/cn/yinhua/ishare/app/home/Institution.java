package cn.yinhua.ishare.app.home;

/**
 * Created by Edward on 8/5/14.
 */
public class Institution {
    /**
     * area code.
     */
    private String ac;


    /**
     * institution type
     */
    private String it;

    /**
     * institution name
     */
    private  String in;

    /**
     * institution address
     */
    private String ia;

    /**
     * institution register
     */
    private String ir;


    /**
     * institution subs
     */
    private String subs;

    /**
     *
     * @return area code
     */
    public String getAc() {
        return ac;
    }

    public void setAc(String ac) {
        this.ac = ac;
    }



    /**
     * Get institution type
     * @return institution type
     */
    public String getIt() {
        return it;
    }

    public void setIt(String it) {
        this.it = it;
    }

    public String getIn() {
        return in;
    }

    public void setIn(String in) {
        this.in = in;
    }

    public String getIa() {
        return ia;
    }

    public void setIa(String ia) {
        this.ia = ia;
    }

    public String getIr() {
        return ir;
    }

    public void setIr(String ir) {
        this.ir = ir;
    }

    public String getSubs() {
        return subs;
    }

    public void setSubs(String subs) {
        this.subs = subs;
    }
}
