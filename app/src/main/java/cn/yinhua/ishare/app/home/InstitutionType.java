package cn.yinhua.ishare.app.home;

/**
 * Created by Edward on 8/8/14.
 */
public class InstitutionType {
    private String it;
    private String itd;

    public String getIt() {
        return it;
    }

    public void setIt(String it) {
        this.it = it;
    }

    public String getItd() {
        return itd;
    }

    public void setItd(String itd) {
        this.itd = itd;
    }
}
