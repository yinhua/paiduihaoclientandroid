package cn.yinhua.ishare.app.home;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.widget.TextView;

import cn.yinhua.ishare.app.R;

/**
 * Created by Edward on 9/18/14.
 */
public class LocationAffirmDialogFragment extends DialogFragment {

    private String city;
    private String postalCode;
    private TextView locationTextView;

    public LocationAffirmDialogFragment() {
    }
/*
    public LocationAffirmDialogFragment(String city, String postalCode, TextView locationTextView) {
        this.city = city;
        this.postalCode = postalCode;
        this.locationTextView = locationTextView;
    }*/

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public TextView getLocationTextView() {
        return locationTextView;
    }

    public void setLocationTextView(TextView locationTextView) {
        this.locationTextView = locationTextView;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String msg  = getResources().getString(R.string.dialog_loc_affirm);
        msg = msg + city;
        builder.setMessage(msg)
                .setPositiveButton(R.string.dialog_loc_yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        locationTextView.setText(getCity());
                        locationTextView.setTag(getPostalCode());
                        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
                        String prefCityKey = getResources().getString(R.string.constant_preference_string_city);
                        sp.edit().putString(prefCityKey,getPostalCode()+ "," +getCity()).commit();

                    }
                })
                .setNegativeButton(R.string.dialog_loc_no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
