package cn.yinhua.ishare.app.home;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

import cn.yinhua.ishare.app.MainActivity;
import cn.yinhua.ishare.app.R;

/**
 * Created by Edward on 8/18/15.
 */
public class LoginTask extends AsyncTask {

    private MainActivity activity;
    private String phoneNum ;
    private String uid;
    private SharedPreferences sp;

    public LoginTask(MainActivity activity,String phoneNum, String uid,SharedPreferences sp) {
        this.activity = activity;
        this.phoneNum =phoneNum;
        this.uid = uid;
        this.sp = sp;
    }



    @Override
    protected Object doInBackground(Object[] params) {

        String result = "";
        String loginUrl = activity.getString(R.string.login_http_url) + "?msisdn="+phoneNum
                +"&nickName="+uid;
        try{

            HttpGet httpGet=new HttpGet(loginUrl);
            //取得HTTP response
            HttpResponse response=new DefaultHttpClient().execute(httpGet);
            //若状态码为200
            if(response.getStatusLine().getStatusCode()==200){
                //取出应答字符串
                HttpEntity entity=response.getEntity();
                result= EntityUtils.toString(entity, HTTP.UTF_8);
            }
        }catch (ClientProtocolException e) {
            e.printStackTrace();


        } catch (IOException e) {
            e.printStackTrace();

        }

        return result;
    }


    @Override
    protected void onPostExecute(Object o) {
        String result = (String)o;
        if(result.contains(phoneNum)){
            sp.edit().putString(activity.getResources().getString(R.string.preference_user_login_id), uid).commit();
        }else {
            Toast.makeText(activity.getApplicationContext(), "昵称和号码不匹配！", Toast.LENGTH_LONG).show();
        }

        activity.getSupportActionBar().setSelectedNavigationItem(0);
        activity.getSupportActionBar().setSelectedNavigationItem(1);
    }
}
