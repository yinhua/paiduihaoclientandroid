package cn.yinhua.ishare.app.queue;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import cn.yinhua.ishare.app.R;
import cn.yinhua.ishare.search.util.IshareSuggestionAdapter;

/**
 * Created by Edward on 4/22/15.
 */
public class QueueStatusListTask extends AsyncTask<String,Void,String> {

    private Context context;
    private HashMap<String,String> queueStatusMap;
    private ListView listView;
    private String atn;

    public QueueStatusListTask(Context context, String atn,
                               HashMap<String,String> queueStatusMap, ListView listView){

        this.listView = listView;
        this.queueStatusMap = queueStatusMap;
        this.context = context;
        this.atn =atn;

    }

    /**
     * Runs on the UI thread before {@link #doInBackground}.
     *
     * @see #onPostExecute
     * @see #doInBackground
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p/>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected String doInBackground(String... params) {
      //  String atns = params[0];
//        System.out.println(params[1] + ", atn is" +atns + ",desc is "+  queueStatusDescReference.get().getText());

        String getBaseUrl = context.getResources().getString(R.string.queue_status_get_url);
        String getUrl = getBaseUrl + "?insATN=" +atn ;

        String result="";
        try{


            HttpGet httpGet=new HttpGet(getUrl);
            //取得HTTP response
            HttpResponse response=new DefaultHttpClient().execute(httpGet);
            //若状态码为200
            if(response.getStatusLine().getStatusCode()==200){
                //取出应答字符串
                HttpEntity entity=response.getEntity();
                result= EntityUtils.toString(entity, HTTP.UTF_8);
            }
        }catch (ClientProtocolException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();

        }

        return result;
    }

    /**
     * <p>Runs on the UI thread after {@link #doInBackground}. The
     * specified result is the value returned by {@link #doInBackground}.</p>
     * <p/>
     * <p>This method won't be invoked if the task was cancelled.</p>
     *
     * @param s The result of the operation computed by {@link #doInBackground}.
     * @see #onPreExecute
     * @see #doInBackground
     * @see #onCancelled(Object)
     */
    @Override
    protected void onPostExecute(String s) {
        //System.out.println(s);
        Map<String,String> map  = new HashMap<String,String>();
        String[] queueStatus = s.replace("{","").replace("}","").split(",");
        for(String str: queueStatus){
           String[] enti =  str.replace("\"","").split(":");
           if(enti.length == 2){
               map.put(enti[0], enti[1]);
           }
        }

        EditText queueStatusT = (EditText)listView.findViewWithTag(atn);
        if(queueStatusT  != null){

            //System.out.println("number changed to " + map.get("number") +"for " + atn );
            queueStatusT.setText(map.get("number"));
            queueStatusMap.put(atn,map.get("number"));

        }
        String lastupdateuser = "未知";

        if(map.get("lastUpdateUser") != null && !map.get("lastUpdateUser").equals("null")){

            if(map.get("lastUpdateUser").startsWith("null")){

                if(map.get("lastUpdateUser").split("\\$").length == 1)lastupdateuser = "未知";
               else{
                    String num = map.get("lastUpdateUser").split("\\$")[1];
                    if(num.equals("null"))lastupdateuser = "未知";
                    else lastupdateuser = "*" + num.substring(num.length()-4,num.length());
                }

            }else{
                lastupdateuser = map.get("lastUpdateUser").split("\\$")[0];
            }
        }

        TextView queueStatusUpdateBy = (TextView)listView.findViewWithTag(atn+ "author");
        Button queueStatusUpdate = (Button)listView.findViewWithTag(atn+ "button");
        if(queueStatusUpdateBy != null)queueStatusUpdateBy.setText("提供者:" + lastupdateuser);
        if(queueStatusUpdate != null)queueStatusUpdate.setText(R.string.queue_status_update_button);
    }
}
