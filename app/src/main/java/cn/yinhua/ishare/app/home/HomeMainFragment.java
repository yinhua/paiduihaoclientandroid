package cn.yinhua.ishare.app.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import cn.yinhua.ishare.app.MainActivity;
import cn.yinhua.ishare.app.R;
import cn.yinhua.ishare.search.SearchActivity;
import it.sephiroth.android.library.widget.HListView;

/**
 * Created by Edward on 7/14/15.
 */
public class HomeMainFragment extends Fragment {


    private MainActivity mainActivity;
    private String ac;

    public void setAc(String ac) {
        this.ac = ac;
    }

    public MainActivity getMainActivity() {
        return mainActivity;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        final LinearLayout mainPage = (LinearLayout)inflater.inflate(R.layout.home_main, container, false);

        FrameLayout  frameLayout = (FrameLayout)mainPage.findViewById(R.id.favoritesFrame);

        WebView webView = (WebView)inflater.inflate(R.layout.loading_gif, frameLayout, false);

        String gifFilePath = "file:///android_asset/loading.gif";
        //"file://" + "/drawable/loading_bc.gif";

        String data = "<HTML><Div align=\"center\"  margin=\"0px\"><IMG src=\""+gifFilePath+"\" margin=\"0px\"/></Div>";
        webView.loadDataWithBaseURL(gifFilePath, data, "text/html", "utf-8", null);
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);

        frameLayout.addView(webView);



        LinearLayout instTypeLayout = (LinearLayout)mainPage.findViewById(R.id.instTypeLayout);
        final HListView instituteTypesList = (HListView)instTypeLayout.findViewById(R.id.instituteTypes);


        instituteTypesList.setAdapter(new InstTypeAdapter(mainActivity.getApplicationContext(), mainActivity.getInstType()));
        instituteTypesList.setOnItemClickListener(new it.sephiroth.android.library.widget.AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(it.sephiroth.android.library.widget.AdapterView<?> adapterView, View view, int i, long l) {
                Object o = instituteTypesList.getItemAtPosition(i);
                InstitutionType inst = (InstitutionType)o;

                Intent intent = new Intent(mainActivity.getApplicationContext(),SearchActivity.class);
                intent.putExtra("cityStr", mainActivity.getLocationTextView().getTag() + "," + mainActivity.getLocationTextView().getText() );
                intent.putExtra("typeSearch","typeSearch");
                intent.putExtra("searchKey",inst.getItd());
                startActivityForResult(intent, getResources().getInteger(R.integer.request_code_search));

            }
        } );


        GetFavoritesTask gft = new GetFavoritesTask();
        gft.setActivity(mainActivity);
        gft.setFrameLayout(frameLayout);
        gft.setInflater(inflater);
        gft.setAc(ac);

        gft.execute();


        return mainPage;
    }
}
