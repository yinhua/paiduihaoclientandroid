package cn.yinhua.ishare.app.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cn.yinhua.ishare.app.R;

/**
 * Created by Edward on 8/8/14.
 */
public class InstTypeAdapter extends BaseAdapter {

    private List<InstitutionType> listData;

    private LayoutInflater layoutInflater;


    public InstTypeAdapter(Context context, List<InstitutionType> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int i) {
        return listData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if(view == null){
            view = layoutInflater.inflate(R.layout.institution_type_list, null);
            viewHolder  = new ViewHolder();
            viewHolder.instTypesImg = (ImageView)view.findViewById(R.id.instTypeImgInITL);
            viewHolder.instNameTxt =  (TextView)view.findViewById(R.id.itDesInTL);
            view.setTag(viewHolder);

        }else {
            viewHolder = (ViewHolder)view.getTag();
        }

        String institutionType = listData.get(position).getIt();

        if(institutionType.equals("1")){
            // hospital
            viewHolder.instTypesImg.setImageResource(R.drawable.hospital);
        }else if(institutionType.equals("3")){
            //bank
            viewHolder.instTypesImg.setImageResource(R.drawable.bank);
        }else if(institutionType.equals("2")){
            //restaurant
            viewHolder.instTypesImg.setImageResource(R.drawable.restaurant);
        }else {
            // others
            viewHolder.instTypesImg.setImageResource(R.drawable.others);
        }

        viewHolder.instNameTxt.setText(listData.get(position).getItd());

        return view;
    }

    static class ViewHolder {
        ImageView instTypesImg;
        TextView instNameTxt;

    }
}
