package cn.yinhua.ishare.app.home;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import cn.yinhua.ishare.app.R;

/**
 * Created by Edward on 7/21/15.
 */
public class FavorInsDeleteDialog extends DialogFragment {

    private ListView favoriates;
    private int  position;
    private Resources resources;
    private SharedPreferences sp;

    public void setFavoriates(ListView favoriates) {
        this.favoriates = favoriates;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setResources(Resources resources) {
        this.resources = resources;
    }

    public void setSp(SharedPreferences sp) {
        this.sp = sp;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("是否删除")
                .setPositiveButton("是", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Object o = favoriates.getItemAtPosition(position);
                        Institution newsData = (Institution) o;

                        String insATNA =  newsData.getAc() + "-" + newsData.getIt()
                                + "$" + newsData.getIn()
                                + "$" + newsData.getIa();
                        String insATND = insATNA
                                + "$" + newsData.getSubs()
                                + "$" + newsData.getIr();


                        String prefInsKey = resources.getString(R.string.preference_favorites_ins_set);
                        String favoritesStr = sp.getString(prefInsKey, null);

                        String favoritesLatest = "";
                        if (favoritesStr != null) {
                            if (favoritesStr.equals(insATND)) {
                                sp.edit().remove(prefInsKey).commit();

                            } else {

                                String[] favoritesItems = favoritesStr.split("\\|");
                                boolean isFirstTime = true;
                                for (String favoItem : favoritesItems) {

                                    if (!favoItem.contains(insATNA)) {
                                        if (isFirstTime) {
                                            favoritesLatest = favoItem;
                                        } else {
                                            favoritesLatest = favoritesLatest + "|" + favoItem;
                                        }
                                        isFirstTime = false;
                                    }
                                }
                                sp.edit().putString(prefInsKey, favoritesLatest).commit();

                            }


                            InstListAdapter adp = (InstListAdapter) favoriates.getAdapter();

                            final ArrayList<Institution> data = new ArrayList<Institution>();

                            if (favoritesLatest != null && favoritesLatest.length() != 0){

                                String[] favoritesItems = favoritesLatest.split("\\|");
                                for(String favoItem : favoritesItems){

                                    String[] insInfArr = favoItem.split("\\$");

                                    String insAT = insInfArr[0];
                                    String name = insInfArr[1];
                                    String address =  insInfArr[2];
                                    String subs = insInfArr[3];
                                    String register = insInfArr[4];
                                    Institution newIns = new Institution();
                                    newIns.setAc(insAT.split("-")[0]);
                                    newIns.setIt(insAT.split("-")[1]);
                                    newIns.setIn(name);
                                    newIns.setIa(address);
                                    newIns.setSubs(subs);
                                    newIns.setIr(register);
                                    data.add(newIns);

                                }
                            }

                            adp.setListData(data);
                            adp.notifyDataSetChanged();
                        }
                    }

                }).setNegativeButton("否", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        return builder.create();
    }

}
