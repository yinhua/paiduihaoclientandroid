package cn.yinhua.ishare.app.queue;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import cn.yinhua.ishare.app.R;

/**
 * Created by Edward on 4/24/15.
 */
public class SubsUpdateDialog extends DialogFragment {

    public static interface FinishActivity{
        public void doFinish(String insATN);
    }

    private String subs;

    private Button button;

    private String instInfo;

    private FinishActivity finishActivity;

    public String getInstInfo() {
        return instInfo;
    }

    public void setInstInfo(String instInfo) {
        this.instInfo = instInfo;
    }

    public String getSubs() {
        return subs;
    }

    public void setSubs(String subs) {
        this.subs = subs;
    }

    public Button getButton() {
        return button;
    }

    public void setButton(Button button) {
        this.button = button;
    }

    public FinishActivity getFinishActivity() {
        return finishActivity;
    }

    public void setFinishActivity(FinishActivity finishActivity) {
        this.finishActivity = finishActivity;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final ArrayList<String> sublist = new ArrayList<String>();
        subs = subs.replace("，",",");

        String subCom = "";
        String [] subsar = subs.split(",");
        for(String sub : subsar){
            if(sub !=null && sub.length() > 0){
                sublist.add(sub);
            }
        }
        String msg  = getResources().getString(R.string.dialog_subs_affirm);
        int count = 1;

        for(String str :sublist){
            msg = msg + "\n\r" +count + ","+ str;
            if(count == 1)subCom = str  ;
            else subCom = subCom + ";" + str;
            count ++;
        }

        final String subsUpdate = subCom;
        final SubsUpdateDialog subDia = this;
        builder.setMessage(msg)
                .setPositiveButton(R.string.dialog_subs_yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        button.setText(R.string.queue_status_subs_submit_process_dec);


                        String[] insInfArr = instInfo.split("\\$");

                        String insAT = insInfArr[0];
                        String name = insInfArr[1];
                        String address =  insInfArr[2];
                        String subs = subsUpdate;
                        String author = insInfArr[4];

                        new SubsUpdateTask(insAT,name,address,null,author,subs,finishActivity,button,subDia).execute();

                    }
                })
                .setNegativeButton(R.string.dialog_subs_no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        button.setText(getResources().getString(R.string.queue_status_subs_submit));
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }



}
