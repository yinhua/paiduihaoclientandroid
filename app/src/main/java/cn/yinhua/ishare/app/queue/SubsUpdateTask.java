package cn.yinhua.ishare.app.queue;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import cn.yinhua.ishare.app.R;

/**
 * Created by Edward on 8/19/15.
 */
public class SubsUpdateTask extends AsyncTask {


    private String insAT;
    private String name;
    private String address;
    private String desc;
    private String author;
    private String subs;
    private SubsUpdateDialog.FinishActivity activity;
    private Button button;
    private SubsUpdateDialog dialog;


    public SubsUpdateTask( String insAT, String name, String address, String desc, String author,
                           String subs, SubsUpdateDialog.FinishActivity activity, Button button,
                           SubsUpdateDialog dialog){
        this.insAT = insAT;
        this.name = name;
        this.address = address;
        this.desc =desc;
        this.author = author;
        this.subs = subs;
        this.activity = activity;
        this.button = button;
        this.dialog = dialog;

    }

    @Override
    protected Object doInBackground(Object[] params) {


        try{
            String insPostUrl = dialog.getString(cn.yinhua.ishare.institution.R.string.institution_http_url);

            DefaultHttpClient httpclient = new DefaultHttpClient();

            //url with the post data
            HttpPost httpost = new HttpPost(insPostUrl);

            //convert parameters into JSON object
            JSONObject holder = new JSONObject();
            holder.put("insAT", insAT);
            holder.put("name",name);
            holder.put("address",address);

            if(desc == null || !desc.startsWith(name))desc = name + ":" + desc;

            if(author != null && author.length() > 0)holder.put("author",author);
            if(desc != null && desc.length() > 0)holder.put("desc",desc);
            if(subs != null && subs.length() > 0)holder.put("subs",subs);

            //passes the results to a string builder/entity

            StringEntity se = new StringEntity(holder.toString(), HTTP.UTF_8);

            //sets the post request as the resulting string
            httpost.setEntity(se);
            //sets a request header so the page receving the request
            //will know what to do with it
            httpost.setHeader("Accept", "application/json");
            httpost.setHeader("Content-type", "application/json");

            //Handles what is returned from the page
            ResponseHandler responseHandler = new BasicResponseHandler();

            httpclient.execute(httpost, responseHandler);
            return  true;

        }catch (Exception e){
            Toast.makeText(dialog.getActivity().getApplicationContext(), R.string.queue_status_subs_add_err_msg, Toast.LENGTH_LONG).show();
            Log.e("institute subs update failed:", Log.getStackTraceString(e));
            return false;
        }

    }

    @Override
    protected void onPostExecute(Object o) {
        Boolean successful = (Boolean)o;
        button.setText("提交");
       // button.setText(dialog.getString(R.string.queue_status_subs_submit));

        if(successful){

            activity.doFinish(insAT + "$" + name + "$" + address + "$"
                    + subs + "$" + author);
        }

    }

}
