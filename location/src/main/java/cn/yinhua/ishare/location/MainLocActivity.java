package cn.yinhua.ishare.location;

import android.app.ActionBar;
import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cn.yinhua.ishare.location.util.CharacterParser;
import cn.yinhua.ishare.location.util.City;
import cn.yinhua.ishare.location.util.CityListAdapter;
import cn.yinhua.ishare.location.util.PinyinComparator;
import cn.yinhua.ishare.location.view.ClearEditText;
import cn.yinhua.ishare.location.view.SideBar;
import it.sephiroth.android.library.widget.HListView;


public class MainLocActivity extends ActionBarActivity {

    private List<City>  cityList;
    private CityListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_loc);
        LinearLayout citiesListLO = (LinearLayout)findViewById(R.id.citiesListLinearLayout);
        //LinearLayout metroCLO = (LinearLayout)citiesListLO.findViewById(R.id.metroCitiesLinearLayout);
        final HListView metroCitiesList = (HListView) citiesListLO.findViewById(R.id.metroCitiesList);

        final String[] municipalityArr = getResources().getStringArray(R.array.municipality);
        String[] municipality = new String[municipalityArr.length];
        for(int i=0; i<municipalityArr.length; i++){
            municipality[i] = municipalityArr[i].split(",")[1];
        }

        metroCitiesList.setAdapter(new ArrayAdapter<String>(
                getSupportActionBar().getThemedContext(),
                R.layout.municipality_item,
                R.id.municipalityTxV,
                municipality));
        metroCitiesList.setOnItemClickListener(new it.sephiroth.android.library.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(it.sephiroth.android.library.widget.AdapterView<?> adapterView, View view, int position, long l) {
                String cityStr  = municipalityArr[position];
                Intent itt = new Intent();
                itt.putExtra("cityStr",cityStr);
                setResult(RESULT_OK,itt);
                finish();
            }
        });

        final ListView citiesList  = (ListView)citiesListLO.findViewById(R.id.citiesList);
        citiesList.setAdapter(getCityAdapter());
        citiesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                City cityChosen = (City)adapter.getItem(position);
                Intent itt = new Intent();
                itt.putExtra("cityStr",cityChosen.getPostalCode() + "," + cityChosen.getName());
                setResult(RESULT_OK,itt);
                finish();

            }
        });

        ClearEditText clTxt = (ClearEditText)citiesListLO.findViewById(R.id.filter_edit);
        clTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filterData(s.toString());
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });



        SideBar sideBar = (SideBar)citiesListLO.findViewById(R.id.sidrbar);
        TextView dialog = (TextView)citiesListLO.findViewById(R.id.dialog);
        sideBar.setTextView(dialog);
        sideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {

            @Override
            public void onTouchingLetterChanged(String s) {
                int position = adapter.getPositionForSection(s.charAt(0));
                if(position != -1){
                    citiesList.setSelection(position);
                }

            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_loc, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case android.R.id.home:
/*                Intent itt = new Intent();
                itt.putExtra("cityStr","");
                setResult(RESULT_OK,itt);
                finish();*/
/*                Intent upIntent = NavUtils.getParentActivityIntent(this);// or using specific class MainActivity.
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    upIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    NavUtils.navigateUpTo(this, upIntent);
                }*/
                setResult(RESULT_CANCELED);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }


    }


    public CityListAdapter  getCityAdapter(){

            String[] citiesArray = getResources().getStringArray(R.array.cities);
            cityList = new ArrayList<City>();

            for(int i=0; i < citiesArray.length; i++){
                String[] cityArr = citiesArray[i].split(",");

                City city = new City();
                city.setPostalCode(cityArr[0]);
                city.setName(cityArr[1]);
                String pinyin = CharacterParser.getInstance().getSelling(cityArr[1]);
                String firstLetter = pinyin.substring(0, 1).toUpperCase();

                if(firstLetter.matches("[A-Z]")){
                    city.setFirstLetter(firstLetter);
                }else{
                    city.setFirstLetter("#");
                }

                cityList.add(city);
            }
            Collections.sort(cityList, new PinyinComparator());
            adapter = new CityListAdapter(cityList,this);
            return  adapter;
    }

    private void filterData(String filterStr){
        List<City> filterDataList = new ArrayList<City>();

        if(TextUtils.isEmpty(filterStr)){
            filterDataList = cityList;
        }else{
            filterDataList.clear();
            for(City city : cityList){
                String name = city.getName();
                if(name.indexOf(filterStr.toString()) != -1 || CharacterParser.getInstance().getSelling(name).startsWith(filterStr.toString())){
                    filterDataList.add(city);
                }
            }
        }

        Collections.sort(filterDataList, new PinyinComparator());
        adapter.updateListView(filterDataList);
    }
}
