package cn.yinhua.ishare.institution;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;







public class InstituionCrActivity extends ActionBarActivity {
    private static final String[] typeArr={"医院","餐馆","银行","其他"};
    private Spinner types;
    private String city;
    private TextView insName;
    private TextView insAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        city = getIntent().getExtras().getString("cityStr");


        setContentView(R.layout.activity_instituion_cr);
        types  = (Spinner)findViewById(R.id.institution_type_spinner);
        ArrayAdapter adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,typeArr);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        types.setAdapter(adapter);

        TextView loc = (TextView)findViewById(R.id.institution_location);
        loc.setText(city.split(",")[1]);

        final String areaCode = city.split(",")[0];
        TelephonyManager tMgr =(TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
        final String mPhoneNumber = tMgr.getLine1Number();


        insName = (TextView)findViewById(R.id.institution_name);
        insAdd = (TextView)findViewById(R.id.institution_address);

        final InstituionCrActivity activity = this;
        Button button = (Button)findViewById(R.id.inst_ctr_submit);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos  = 1 + types.getSelectedItemPosition();

                String iname = insName.getText().toString();
                String iadd = insAdd.getText().toString();

                if(iname == null || iadd == null || iname.length() ==0 || iadd.length() ==0){

                    Toast.makeText(getBaseContext(), R.string.institution_cr_check_null_msg,Toast.LENGTH_LONG).show();
                    return;
                }

                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String author = sp.getString(getResources().getString(R.string.preference_user_login_id), "null");


                if(mPhoneNumber !=  null){
                    String pnu = mPhoneNumber.replace("+", "");
                    author = author + "-" + pnu;
                }
                else  author = author + "-" + "null";

//                try{
//                    Object ins = createInstitution(areaCode + "-" + pos,  iname,  iadd,  null,
//                            author, null);
//                }catch (Exception e){
//                    Toast.makeText(getBaseContext(), R.string.institution_cr_error,Toast.LENGTH_LONG).show();
//                    Log.e("institute created failed:",Log.getStackTraceString(e));
//                    return;
//                }
//
//                Intent itt = new Intent();
//                itt.putExtra("insATND", areaCode+ "-" + pos + "$" + iname + "$" +iadd + "$"+ "null"+"$"+ author);
//                setResult(RESULT_OK,itt);
//                finish();

                new InstCreateTask(areaCode + "-" + pos,  iname,  iadd,  null,author, null, activity).execute();

            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.instituion_cr, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



}
