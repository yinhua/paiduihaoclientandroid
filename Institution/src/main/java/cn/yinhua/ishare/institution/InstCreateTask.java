package cn.yinhua.ishare.institution;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

/**
 * Created by Edward on 8/18/15.
 */
public class InstCreateTask extends AsyncTask {

    private  String insAT;
    private String name;
    private String address;
    private String desc;
    private String author;
    private String subs;
    private ActionBarActivity activity;

    public InstCreateTask( String insAT, String name, String address, String desc, String author,
                            String subs, ActionBarActivity activity){
        this.insAT = insAT;
        this.name = name;
        this.address = address;
        this.desc =desc;
        this.author = author;
        this.subs = subs;
        this.activity = activity;

    }

    @Override
    protected Object doInBackground(Object[] params) {


        try{
            String insPostUrl = activity.getString(R.string.institution_http_url);

            DefaultHttpClient httpclient = new DefaultHttpClient();

            //url with the post data
            HttpPost httpost = new HttpPost(insPostUrl);

            //convert parameters into JSON object
            JSONObject holder = new JSONObject();
            holder.put("insAT", insAT);
            holder.put("name",name);
            holder.put("address",address);

            if(desc == null || !desc.startsWith(name))desc = name + ":" + desc;

            if(author != null && author.length() > 0)holder.put("author",author);
            if(desc != null && desc.length() > 0)holder.put("desc",desc);
            if(subs != null && subs.length() > 0)holder.put("subs",subs);

            //passes the results to a string builder/entity

            StringEntity se = new StringEntity(holder.toString(), HTTP.UTF_8);

            //sets the post request as the resulting string
            httpost.setEntity(se);
            //sets a request header so the page receving the request
            //will know what to do with it
            httpost.setHeader("Accept", "application/json");
            httpost.setHeader("Content-type", "application/json");

            //Handles what is returned from the page
            ResponseHandler responseHandler = new BasicResponseHandler();
            return httpclient.execute(httpost, responseHandler);

        }catch (Exception e){
            Toast.makeText(activity.getBaseContext(), R.string.institution_cr_error, Toast.LENGTH_LONG).show();
            Log.e("institute created failed:", Log.getStackTraceString(e));

        }

        return  null;
    }

    @Override
    protected void onPostExecute(Object o) {

        Intent itt = new Intent();
        itt.putExtra("insATND", insAT + "$" + name + "$" +address + "$"+ "null"+"$"+ author);
        activity.setResult(activity.RESULT_OK, itt);
        activity.finish();

    }
}
