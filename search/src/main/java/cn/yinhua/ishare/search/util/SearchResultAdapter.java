package cn.yinhua.ishare.search.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import cn.yinhua.ishare.search.R;

/**
 * Created by Edward on 4/7/15.
 */
public class SearchResultAdapter extends BaseAdapter {


    private List<String> instrArray;

    private LayoutInflater layoutInflater;


    public SearchResultAdapter(Context context, List<String> instrArray){
        this.instrArray = instrArray;
        layoutInflater =  LayoutInflater.from(context);
    }

    /**
     * How many items are in the data set represented by this Adapter.
     *
     * @return Count of items.
     */
    @Override
    public int getCount() {
        return instrArray.size();
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     *                 data set.
     * @return The data at the specified position.
     */
    @Override
    public Object getItem(int position) {
        return instrArray.get(position);
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * {@link android.view.LayoutInflater#inflate(int, android.view.ViewGroup, boolean)}
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position    The position of the item within the adapter's data set of the item whose view
     *                    we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     *                    is non-null and of an appropriate type before using. If it is not possible to convert
     *                    this view to display the correct data, this method can create a new view.
     *                    Heterogeneous lists can specify their number of view types, so that this View is
     *                    always of the right type (see {@link #getViewTypeCount()} and
     *                    {@link #getItemViewType(int)}).
     * @param parent      The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        {
            ViewHolder viewHolder;
            if(convertView == null){
                convertView = layoutInflater.inflate(R.layout.search_result_ins, null);
                viewHolder  = new ViewHolder();
                viewHolder.instTypesImg = (ImageView)convertView.findViewById(R.id.instTypesImg);
                viewHolder.instAdsTxt = (TextView)convertView.findViewById(R.id.instAdsTxt);
                viewHolder.instNameTxt =  (TextView)convertView.findViewById(R.id.instNameTxt);
                convertView.setTag(viewHolder);

            }else {
                viewHolder = (ViewHolder)convertView.getTag();
            }

            HashMap<String,String> instMap = new HashMap<String, String>();
            String instStr = instrArray.get(position);
           // System.out.println(instStr);
            String[] items = instStr.split(",");
            for(int len = 0; len <items.length; len++){
                String item = items[len].replace("\"", "");
               // System.out.println(item);
                String entity[] = item.split(":");

                if(entity.length == 2)instMap.put(entity[0],entity[1]);
            }



            String institutionType = instMap.get("insAT").split("-")[1];

            if(institutionType.equals("1")){
                // hospital
                viewHolder.instTypesImg.setImageResource(R.drawable.hospital);
            }else if(institutionType.equals("3")){
                //bank
                viewHolder.instTypesImg.setImageResource(R.drawable.bank);
            }else if(institutionType.equals("2")){
                //restaurant
                viewHolder.instTypesImg.setImageResource(R.drawable.restaurant);
            }else {
                // others
                viewHolder.instTypesImg.setImageResource(R.drawable.others);
            }

            viewHolder.instNameTxt.setText(instMap.get("name"));
            viewHolder.instAdsTxt.setText(instMap.get("address"));


            return convertView;
        }
    }

    static class ViewHolder {
        ImageView instTypesImg;
        TextView instNameTxt;
        TextView instAdsTxt;

    }
}
