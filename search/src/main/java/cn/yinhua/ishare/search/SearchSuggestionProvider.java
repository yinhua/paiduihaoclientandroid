package cn.yinhua.ishare.search;

import android.content.SearchRecentSuggestionsProvider;

/**
 * Created by Edward on 9/24/14.
 */
public class SearchSuggestionProvider extends SearchRecentSuggestionsProvider {
    public final static String AUTHORITY = "cn.yinhua.ishare.search.searchSuggestionProvider";
    public final static int MODE = DATABASE_MODE_QUERIES;

    public SearchSuggestionProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }


}
