package cn.yinhua.ishare.search;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Point;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.provider.SearchRecentSuggestions;
import android.support.v4.app.NavUtils;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.HeaderViewListAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

import java.util.ArrayList;



import cn.yinhua.ishare.search.util.IshareSuggestionAdapter;
import cn.yinhua.ishare.search.util.SearchResultAdapter;


public class SearchActivity extends ActionBarActivity implements SearchView.OnQueryTextListener {

    private SearchView sv;
    private ListView lv;
    private SearchableInfo searchableInfo;
    private String city;
    private TextView searchActionTitle;
    private ListView searchResult;
    //自动完成的列表


    private SearchRecentSuggestions suggestions;

    private static final int MSG_SUCCESS = 0;
    private static final int MSG_FAILURE = 1;
    private static final int MSG_NODATA = 2;


    private Handler sear_r_handler = new Handler() {
        /**
         * Subclasses must implement this to receive messages.
         *
         * @param msg
         */
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) {

                case MSG_SUCCESS:
                    searchActionTitle.setText(R.string.search_result_successful);
                    searchResult.setVisibility(View.VISIBLE);
                    searchResult.setAdapter(new SearchResultAdapter(getApplicationContext(),(ArrayList)msg.obj));
                    break;

                case MSG_FAILURE:
                    searchActionTitle.setText(R.string.search_result_failure);
                    searchResult.setVisibility(View.GONE);
                    break;

                case MSG_NODATA:
                    searchActionTitle.setText(R.string.search_result_null);
                    searchResult.setVisibility(View.GONE);
                    break;
            }
        }
    };

    //用户输入字符时激发该方法
    @Override
    public boolean onQueryTextChange(String newText) {
        // TODO Auto-generated method stub
/*        if(TextUtils.isEmpty(newText))
        {
            //清楚ListView的过滤
            lv.clearTextFilter();
        }
        else
        {
            //使用用户输入的内容对ListView的列表项进行过滤
            lv.setFilterText(newText);

        }*/
        HeaderViewListAdapter adapter = (HeaderViewListAdapter)lv.getAdapter();

        adapter.getFilter().filter(newText);
        searchActionTitle.setText(R.string.search_history);
        lv.setVisibility(View.VISIBLE);
        searchResult.setVisibility(View.GONE);
        return true;
    }


    @Override
    public boolean onQueryTextSubmit(String query) {

       // Toast.makeText(this, "您选择的是：" + query, Toast.LENGTH_SHORT).show();
        saveHis(query);
        return false;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        suggestions = new SearchRecentSuggestions(this,
                SearchSuggestionProvider.AUTHORITY, SearchSuggestionProvider.MODE);
        SearchManager searchManager =  (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchableInfo = searchManager.getSearchableInfo(getComponentName());

        setContentView(R.layout.activity_search);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        searchActionTitle =(TextView)findViewById(R.id.search_action_tile);
        searchResult = (ListView)findViewById(R.id.search_result_list);
        lv=(ListView)findViewById(R.id.search_history_list);
        View footerView = ((LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.search_history_clear, null, false);
        lv.addFooterView(footerView);

        lv.setTextFilterEnabled(true);


        IshareSuggestionAdapter adapter = new IshareSuggestionAdapter(this.getApplicationContext(),null,true, searchableInfo);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == lv.getAdapter().getCount() - 1 ) {//clear history
                    suggestions.clearHistory();
                    HeaderViewListAdapter hwld = (HeaderViewListAdapter)lv.getAdapter();
                    hwld.getFilter().filter("");
                } else {
                    IshareSuggestionAdapter.ViewHolder viewHolder = (IshareSuggestionAdapter.ViewHolder) view.getTag();
                    String searchKey = viewHolder.searchKeyword.getText().toString();
                    launchQuerySearch(KeyEvent.KEYCODE_UNKNOWN, null,  searchKey);
                    Log.i("searchKey:",searchKey);
                }
            }
        });

        adapter.getFilter().filter("");


        searchResult.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            /**
             * Callback method to be invoked when an item in this AdapterView has
             * been clicked.
             * <p/>
             * Implementers can call getItemAtPosition(position) if they need
             * to access the data associated with the selected item.
             *
             * @param parent   The AdapterView where the click happened.
             * @param view     The view within the AdapterView that was clicked (this
             *                 will be a view provided by the adapter)
             * @param position The position of the view in the adapter.
             * @param id       The row id of the item that was clicked.
             */
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String selectedStr  = (String)searchResult.getAdapter().getItem(position);
                Intent intent = new Intent();
                intent.putExtra("searchResult",selectedStr);
                setResult(666,intent);

                finish();
            }
        });

        Intent intent = getIntent();
        city = intent.getExtras().getString("cityStr");
        handleIntent(getIntent());

        String typeSearch = intent.getExtras().getString("typeSearch");
        if(typeSearch != null && typeSearch.equals("typeSearch")){
            String searchKey = intent.getExtras().getString("searchKey");
            launchQuerySearch(KeyEvent.KEYCODE_UNKNOWN, null,  searchKey);
        }

    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.i("on new Intent:","on new Intent");
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        Log.i("++++++++++++++","" + city);

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            searchActionTitle.setText(R.string.search_result_title);
            lv.setVisibility(View.GONE);



            String areaCode = city.split(",")[0];

            QueryInstitute qi = new QueryInstitute(sear_r_handler ,  query ,  areaCode);



            Thread tt = new Thread(qi);
            tt.start();

        }else{
            searchResult.setVisibility(View.GONE);
        }

    }

    private void saveHis(String query){

        SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                SearchSuggestionProvider.AUTHORITY, SearchSuggestionProvider.MODE);
        suggestions.saveRecentQuery(query, null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search, menu);
        final MenuItem item = menu.findItem(R.id.institutionSearchAction);
        final SearchView searchView = (SearchView)MenuItemCompat.getActionView(item);

        searchView.setOnQueryTextListener(this);

        searchView.setIconifiedByDefault(false);

        SearchManager searchManager =  (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.requestFocus();



        LinearLayout ll = (LinearLayout)searchView.getChildAt(0);
        LinearLayout ll2 = (LinearLayout)ll.getChildAt(2);
        LinearLayout ll3 = (LinearLayout)ll2.getChildAt(1);
        SearchView.SearchAutoComplete myAutoCompleteTextView = ((SearchView.SearchAutoComplete)ll3.getChildAt(0));

        myAutoCompleteTextView.setThreshold(Integer.MAX_VALUE);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }else if(id == android.R.id.home){

            setResult(RESULT_CANCELED);
            finish();
            return true;
/*            Log.i("navigate up"," lalalala.");
            NavUtils.navigateUpFromSameTask(this);
            return true;*/
        }

        return super.onOptionsItemSelected(item);
    }


    private void launchQuerySearch(int actionKey, String actionMsg, String query) {
        String action = Intent.ACTION_SEARCH;
        Intent intent = createIntent(action, query, actionKey, actionMsg);

        startActivityForResult(intent, 1);
        //getApplicationContext().startActivity(intent,1);
    }

    private Intent createIntent(String action, String query,
                                int actionKey, String actionMsg) {
        // Now build the Intent
        Intent intent = new Intent(action);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        // We need CLEAR_TOP to avoid reusing an old task that has other activities
        // on top of the one we want. We don't want to do this in in-app search though,
        // as it can be destructive to the activity stack.


        intent.putExtra(SearchManager.QUERY, query);



        if (actionKey != KeyEvent.KEYCODE_UNKNOWN) {
            intent.putExtra(SearchManager.ACTION_KEY, actionKey);
            intent.putExtra(SearchManager.ACTION_MSG, actionMsg);
        }
        intent.setComponent(searchableInfo.getSearchActivity());
        return intent;
    }

     class QueryInstitute implements Runnable {
        Handler handler;
        String name;
        String areaCode;

        QueryInstitute(Handler handler, String name , String areaCode){
            this.handler = handler;
            this.name = name;
            this.areaCode = areaCode;
        }

        public void run(){

            ArrayList<String> insts = new ArrayList<String>();
            boolean isFailure = false;
            for(int i =1; i<=4;i ++){
                String AT= areaCode + "-" + i;
                String getBaseUrl = getString(R.string.institution_search_url);
                String getUrl = getBaseUrl + "?areaTypeCode=" +AT + "&name="+ name + "&filter=true";

                String result="";
                try{

 
                    HttpGet httpGet=new HttpGet(getUrl);
                    //取得HTTP response
                    HttpResponse response=new DefaultHttpClient().execute(httpGet);
                    //若状态码为200
                    if(response.getStatusLine().getStatusCode()==200){
                        //取出应答字符串
                        HttpEntity entity=response.getEntity();
                        result= EntityUtils.toString(entity, HTTP.UTF_8);
                    }
                }catch (ClientProtocolException e) {
                    e.printStackTrace();
                    isFailure = true;
                    break;
                } catch (IOException e) {
                    e.printStackTrace();
                    isFailure = true;
                    break;
                }


                if(result.length() > 2){// == 2 ,"[]"
                    String[] instArr = result.split("\\},");
                    for(int len = 0; len<instArr.length ; len++){
                        String insti = instArr[len].replace("[","").replace("]","").replace("{","").replace("}","");
                        insts.add(insti);
                    }
                }
            }


            if(isFailure){
                handler.obtainMessage(MSG_FAILURE,insts).sendToTarget();
            }else if(insts.size() == 0){
                handler.obtainMessage(MSG_NODATA,insts).sendToTarget();
            }
            else handler.obtainMessage(MSG_SUCCESS,insts).sendToTarget();

        }

    }


}
