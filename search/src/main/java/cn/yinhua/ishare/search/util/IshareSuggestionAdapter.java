package cn.yinhua.ishare.search.util;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.yinhua.ishare.search.R;

/**
 * Created by Edward on 10/17/14.
 */
public class IshareSuggestionAdapter extends CursorAdapter{

    private  Context context;
    private SearchableInfo searchable;
    /**
     * Constructor that allows control over auto-requery.  It is recommended
     * you not use this, but instead {@link #CursorAdapter(android.content.Context, android.database.Cursor, int)}.
     * When using this constructor, {@link #FLAG_REGISTER_CONTENT_OBSERVER}
     * will always be set.
     *
     * @param context     The context
     * @param c           The cursor from which to get the data.
     * @param autoRequery If true the adapter will call requery() on the
     *                    cursor whenever it changes so the most recent
     */
    public IshareSuggestionAdapter(Context context, Cursor c, boolean autoRequery, SearchableInfo searchable ) {
        super(context, c, autoRequery);
        this.context = context;
        this.searchable = searchable;
    }





    /**
     * Makes a new view to hold the data pointed to by cursor.
     *
     * @param context Interface to application's global information
     * @param cursor  The cursor from which to get the data. The cursor is already
     *                moved to the correct position.
     * @param parent  The parent to which the new view is attached to
     * @return the newly created view.
     */
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        TextView view =null;
        LayoutInflater layoutInflater =  (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout linearLayout =(LinearLayout)layoutInflater.inflate(R.layout.search_history_txtv_layout, parent, false);
        view = (TextView)linearLayout.findViewById(R.id.search_history_txtv);
        ViewHolder vh = new ViewHolder();
        vh.searchKeyword = view;
        linearLayout.setTag(vh);
        return linearLayout;
    }

    /**
     * Bind an existing view to the data pointed to by cursor
     *
     * @param view    Existing view, returned earlier by newView
     * @param context Interface to application's global information
     * @param cursor  The cursor from which to get the data. The cursor is already
     */
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder vh = (ViewHolder)view.getTag();
        TextView keywords = vh.searchKeyword;
        String[] columns  = cursor.getColumnNames();
/*        for(String str : columns){
            Log.i("~~~~~~~~~~~~~~~~~~~~~~~ ",str);
        }
        Log.i("~~~~~~~~~~~~~~~~~~~~~~~ ", "" + cursor.getString(cursor.getColumnIndex("suggest_text_1")));*/
        keywords.setText(cursor.getString(cursor.getColumnIndex("suggest_text_1")));
    }


    public static class ViewHolder {

       public TextView searchKeyword;


    }

    private   Cursor  getSearchManagerSuggestions(String query, int limit, SearchableInfo searchable, Context context) {
        if (searchable == null) {
            return null;
        }

        String authority = searchable.getSuggestAuthority();
        if (authority == null) {
            return null;
        }

        Uri.Builder uriBuilder = new Uri.Builder()
                .scheme(ContentResolver.SCHEME_CONTENT)
                .authority(authority)
                .query("")  // TODO: Remove, workaround for a bug in Uri.writeToParcel()
                .fragment("");  // TODO: Remove, workaround for a bug in Uri.writeToParcel()

        // if content path provided, insert it now
        final String contentPath = searchable.getSuggestPath();
        if (contentPath != null) {
            uriBuilder.appendEncodedPath(contentPath);
        }

        // append standard suggestion query path
        uriBuilder.appendPath(SearchManager.SUGGEST_URI_PATH_QUERY);

        // get the query selection, may be null
        String selection = searchable.getSuggestSelection();
        // inject query, either as selection args or inline
        String[] selArgs = null;
        if (selection != null) {    // use selection if provided
            selArgs = new String[] { query };
        } else {                    // no selection, use REST pattern
            uriBuilder.appendPath(query);
        }

        if (limit > 0) {
            uriBuilder.appendQueryParameter("limit", String.valueOf(limit));
        }

        Uri uri = uriBuilder.build();

        // finally, make the query
        return context.getContentResolver().query(uri, null, selection, selArgs, null);
    }

    /**
     * Runs a query with the specified constraint. This query is requested
     * by the filter attached to this adapter.
     * <p/>
     * The query is provided by a
     * {@link android.widget.FilterQueryProvider}.
     * If no provider is specified, the current cursor is not filtered and returned.
     * <p/>
     * After this method returns the resulting cursor is passed to {@link #changeCursor(android.database.Cursor)}
     * and the previous cursor is closed.
     * <p/>
     * This method is always executed on a background thread, not on the
     * application's main thread (or UI thread.)
     * <p/>
     * Contract: when constraint is null or empty, the original results,
     * prior to any filtering, must be returned.
     *
     * @param constraint the constraint with which the query must be filtered
     * @return a Cursor representing the results of the new query
     * @see #getFilter()
     * @see #getFilterQueryProvider()
     * @see #setFilterQueryProvider(android.widget.FilterQueryProvider)
     */
    @Override
    public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
        {
            String query = (constraint == null) ? "" : constraint.toString();
            /**
             * for in app search we show the progress spinner until the cursor is returned with
             * the results.
             */
            Cursor cursor = null;

            try {
                cursor = getSearchManagerSuggestions(query, 500,  searchable,  context);
                // trigger fill window so the spinner stays up until the results are copied over and
                // closer to being ready
                if (cursor != null) {
                    cursor.getCount();
                    return cursor;
                }
            } catch (RuntimeException e) {

            }
            // If cursor is null or an exception was thrown, stop the spinner and return null.
            // changeCursor doesn't get called if cursor is null
            return null;
        }
    }
}
